echo "Running Tests"
./run_tests.sh

echo ""
echo ""

echo "Running with automatonDefinitions/automatonTestEmptyStackAndFinalState_aabb and 'aabb' string"
./run.sh automatonDefinitions/automatonTestEmptyStackAndFinalState_aabb aabb

echo ""
echo ""

echo "Running with automatonDefinitions/automatonTestEmptyStackAndNotFinalState_ab and 'ab' string"
./run.sh automatonDefinitions/automatonTestEmptyStackAndNotFinalState_ab ab

echo ""
echo ""

echo "Running with automatonDefinitions/automatonTestFinalStateAndNotEmptyStack_aaabbb and 'aaabbb' string"
./run.sh automatonDefinitions/automatonTestFinalStateAndNotEmptyStack_aaabbb aaabbb

echo ""
