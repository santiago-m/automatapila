package automata;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import automata.State;

public class StateTest {
	
	@Test
	public void testConstructor() {
		String nameOfState = "name";
		
		State s = new State(nameOfState);
		
		assertTrue(s.name().equals(nameOfState));
	}
	
	@Test
	public void testRenameMethod() {
		String nameOfState = "name";
		String newName = "another name";
		
		State s = new State(nameOfState);
		s.rename(newName);
		
		assertFalse(s.name().equals(nameOfState));
		assertTrue(s.name().equals(newName));
	}
	
	@Test
	public void testEquals() {
		String nameOfState = "name";
		State s = new State(nameOfState);
		
		//Prove A.getClass() != B.getClass => not(A.equals(B))
		Object anotherObject = new Object();
		assertFalse(s.equals(anotherObject));
		
		//Prove A.equals(A)
		assertTrue(s.equals(s));
		
		//Prove A.name == B.name => A.equals(B)
		State copyOfS = new State(nameOfState);
		assertTrue(s.equals(copyOfS));
	}

}
