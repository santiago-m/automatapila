package automata;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.*;

import automata.State;
import utils.Quintuple;

public class DFAPilaTest {
	
	//List of States used to build automatons
	State[] listOfStates = new State[] {
			new State("q0"),
			new State("q1"),
			new State("q2"),
	};
	
	//List of Characters used to build automatons
	Character[] listOfChars = new Character[] {
		'a',
		'b'
	};
	
	//List of Strings used to build automatons transitions
	String[] listOfStackStrings = new String[] {
			DFAPila.Lambda.toString(),
			"a",
			"aa",
			"ab",
			"b",
			"bb",
			"ba"
	};
	
	/**
	 * Method that returns an automaton that complies with rep_ok
	 * i.e a correct automaton
	 * */
	private DFAPila getCorrectAutomaton() {
		Set<State> states = new HashSet();
		states.add(listOfStates[0]);
		states.add(listOfStates[1]);
		states.add(listOfStates[2]);
		
		Set<State> finalStates = new HashSet();
		finalStates.add(listOfStates[2]);
		
		Set<Character> alphabet = new HashSet<Character>();
		alphabet.add(listOfChars[0]);
		alphabet.add(listOfChars[1]);
		
		Set<Character> stackAlphabet = new HashSet<Character>();
		stackAlphabet.add('a');
		
		Character stackInitial = '@';
		
		Set<Quintuple<State, Character, Character, String, State>> transitions = new HashSet<Quintuple<State, Character, Character, String, State>>();
		Quintuple<State, Character, Character, String, State> transition;
		
		transition = new Quintuple<State, Character, Character, String, State>(listOfStates[0], listOfChars[0], stackInitial, listOfStackStrings[1], listOfStates[1]);
		transitions.add(transition);
		
		transition = new Quintuple<State, Character, Character, String, State>(listOfStates[1], listOfChars[0], listOfChars[0], listOfStackStrings[2], listOfStates[1]);
		transitions.add(transition);
		
		transition = new Quintuple<State, Character, Character, String, State>(listOfStates[1], listOfChars[1], listOfChars[0], DFAPila.Lambda.toString(), listOfStates[2]);
		transitions.add(transition);
		
		transition = new Quintuple<State, Character, Character, String, State>(listOfStates[2], listOfChars[1], listOfChars[0], DFAPila.Lambda.toString(), listOfStates[2]);
		transitions.add(transition);
		
		DFAPila automata = new DFAPila(states, alphabet, stackAlphabet, transitions, stackInitial, listOfStates[0], finalStates);
		return automata;
	}
	
	@Test
	public void writeAutomThenReadItReturnsOriginalEqual() throws FileNotFoundException {
		DFAPila automata = getCorrectAutomaton();
		
		automata.writeToFile("archivo de prueba");
		DFAPila copy_of_automata = DFAPila.crearDesdeArchivo("archivo de prueba");
		
		assertTrue(automata.equals(copy_of_automata));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRepOkFirstElementNull() {
		Set<State> states = new HashSet<State>();
		Set<Character> alphabet = new HashSet<Character>();
		Set<Quintuple<State, Character, Character, String, State>> transitions = new HashSet<Quintuple<State, Character, Character, String, State>>();
		State state1 = new State("Hello");
		
		DFAPila automata = new DFAPila(null, alphabet, alphabet, transitions, 'a', state1, states);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRepOkFourthElementNull() {
		Set<State> states = new HashSet<State>();
		Set<Character> alphabet = new HashSet<Character>();
		State state1 = new State("Hello");
		
		DFAPila automata = new DFAPila(states, alphabet, alphabet, null, 'a', state1, states);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRepOkFifthElementNull() {
		Set<State> states = new HashSet<State>();
		Set<Character> alphabet = new HashSet<Character>();
		Set<Quintuple<State, Character, Character, String, State>> transitions = new HashSet<Quintuple<State, Character, Character, String, State>>();
		State state1 = new State("Hello");
		
		DFAPila automata = new DFAPila(states, alphabet, alphabet, transitions, null, state1, states);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRepOkSexthElementNull() {
		Set<State> states = new HashSet<State>();
		Set<Character> alphabet = new HashSet<Character>();
		Set<Quintuple<State, Character, Character, String, State>> transitions = new HashSet<Quintuple<State, Character, Character, String, State>>();
		
		DFAPila automata = new DFAPila(states, alphabet, alphabet, transitions, 'a', null, states);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRepOkSeventhElementNull() {
		Set<State> states = new HashSet<State>();
		Set<Character> alphabet = new HashSet<Character>();
		Set<Quintuple<State, Character, Character, String, State>> transitions = new HashSet<Quintuple<State, Character, Character, String, State>>();
		State state1 = new State("Hello");
		
		DFAPila automata = new DFAPila(states, alphabet, alphabet, transitions, 'a', state1, null);
	}
	
	@Test
	public void testRepOkWithInitialStateNameNull() {
		DFAPila automata = getCorrectAutomaton();
		
		automata.initial.rename(null);
		
		assertFalse(automata.rep_ok());
	}
	
	@Test
	public void testRepOkWithCorrectAutomaton() {
		DFAPila automata = getCorrectAutomaton();
		assertTrue(automata.rep_ok());
	}
	
	@Test
	public void testRepOkWithWrongStackInitial() {
		DFAPila automata = getCorrectAutomaton();
		automata.stackInitial = 'B';
		
		assertFalse(automata.rep_ok());
	}
	
	@Test
	public void testRepOkJokerNotInStackAlphabet() {
		DFAPila automata = getCorrectAutomaton();
		
		automata.stackAlphabet.remove(automata.stackInitial);
		assertFalse(automata.rep_ok());
	}
	
	@Test
	public void testRepOkLambdaNotInStackAlphabet() {
		DFAPila automata = getCorrectAutomaton();
		
		automata.stackAlphabet.remove(DFAPila.Lambda);
		assertFalse(automata.rep_ok());
	}
	
	@Test
	public void testRepOkLambdaInStack() {
		DFAPila automata = getCorrectAutomaton();
		
		automata.states.remove(listOfStates[2]);
		assertFalse(automata.rep_ok());
	}
	
	@Test
	public void testRepOkStateInTransitionNotInStatesSet() {
		DFAPila automata = getCorrectAutomaton();
		Iterator<Quintuple<State, Character, Character, String, State>> it;
		Quintuple<State, Character, Character, String, State> firstTransition = null;
		it = automata.transitions.iterator();

		while (it.hasNext()) {
			firstTransition = it.next();
			if (firstTransition.first() != firstTransition.fifth())
				break;
		}
		
		assertTrue(automata.rep_ok());
		
		automata.states.remove(firstTransition.first());
		
		assertFalse(automata.rep_ok());
		
		if (firstTransition.first() != firstTransition.fifth()) {
			automata.states.remove(firstTransition.fifth());
			
			assertFalse(automata.rep_ok());
			
			automata.states.add(firstTransition.first());
			assertFalse(automata.rep_ok());
		}
	}
	
	@Test
	public void testRepOkCharToEarNotInAlphabet() {
		DFAPila automata = getCorrectAutomaton();
		Iterator<Quintuple<State, Character, Character, String, State>> it;
		Quintuple<State, Character, Character, String, State> firstTransition = null;
		it = automata.transitions.iterator();
		
		firstTransition = it.next();
		
		automata.alphabet.remove(firstTransition.second());
		assertFalse(automata.rep_ok());
	}
	
	@Test
	public void testRepOkCharToWriteNotInStackAlphabet() {
		DFAPila automata = getCorrectAutomaton();
		Iterator<Quintuple<State, Character, Character, String, State>> it;
		Quintuple<State, Character, Character, String, State> firstTransition = null;
		it = automata.transitions.iterator();
		
		firstTransition = it.next();
		
		firstTransition.setFourth("mm");
		
		assertFalse(automata.rep_ok());
	}
	
	@Test
	public void testDeltaWithCorrectTransition() {
		DFAPila automata = getCorrectAutomaton();
		Iterator<Quintuple<State, Character, Character, String, State>> it;
		Quintuple<State, Character, Character, String, State> transition = null;
		it = automata.transitions.iterator();
		
		assertTrue(automata.rep_ok());
		
		State newState = null;
		while (it.hasNext()) {
			transition = it.next();
			
			if (transition.third().equals(automata.stackInitial)) {
				newState = automata.delta(transition.first(), transition.second());
				break;
				}
		}
	
		assertTrue(newState == transition.fifth());
		
		newState = automata.delta(transition.first(), 'h');
		
		assertTrue(newState == null);
	}
	
	@Test
	public void testAcceptsFinalState() {
		DFAPila automata = getCorrectAutomaton();
		
		assertTrue(automata.acceptsFinalState("aabb"));
		assertFalse(automata.acceptsFinalState("aa"));
	}
	
	@Test
	public void testAcceptsEmptyStack() {
		DFAPila automata = getCorrectAutomaton();
		
		assertTrue(automata.acceptsEmptyStack("aabb"));
		
		assertFalse(automata.acceptsEmptyStack("aa"));
	}
	
	@Test
	public void testToEmptyStack() {
		DFAPila automata = getCorrectAutomaton();
		String strToProve = "aab";
		
		assertTrue(automata.acceptsFinalState(strToProve));
		assertFalse(automata.acceptsEmptyStack(strToProve));
		
		DFAPila newAutomata = automata.toEmptyStack();
		
		assertTrue(newAutomata.acceptsEmptyStack(strToProve));
		
		
	}
	
	@Test
	public void testToFinalState() {
		DFAPila automata = getCorrectAutomaton();
		
		//Transformo el automata para que acepte la cadena ab por pila vacia y no por estado final
		State newState = new State("q3");
		
		automata.states.add(newState);
		automata.finalStates.remove(listOfStates[2]);
		automata.finalStates.add(newState);
		
		Quintuple<State, Character, Character, String, State> newTransition = 
				new Quintuple<State, Character, Character, String, State>(listOfStates[2], 'b', DFAPila.Initial, DFAPila.Initial.toString(), newState);
		
		automata.transitions.add(newTransition);
		
		String strToProve = "ab";

		assertTrue(automata.acceptsEmptyStack(strToProve));
		assertFalse(automata.acceptsFinalState(strToProve));
		
		DFAPila newAutomata = automata.toFinalState();
		
		assertTrue(newAutomata.acceptsFinalState(strToProve));
	}
}
