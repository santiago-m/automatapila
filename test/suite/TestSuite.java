package suite;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import utils.QuintupleTest;
import automata.StateTest;
import automata.DFAPilaTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	QuintupleTest.class,
	StateTest.class,
	DFAPilaTest.class})
public class TestSuite {

}