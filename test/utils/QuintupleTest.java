package utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import automata.State;
import utils.Quintuple;

public class QuintupleTest {
	
	@Test
	public void testConstructorWithStatesCharactersAndStrings() {
		State first = new State("from state");
		Character second = 'c';
		Character third = 'b';
		String fourth = "cadena 1";
		State fifth = new State("to state");
		
		Quintuple<State, Character, Character, String, State> tuple = new Quintuple<State, Character, Character, String, State>(first, second, third, fourth, fifth);
		
		State realFirst = tuple.first(),
				realFifth = tuple.fifth();
		
		Character realSecond = tuple.second(), 
				realThird = tuple.third();
		
		String realFourth = tuple.fourth();
		
		
		assertTrue(first.name() + " is the spected first element of the tuple. Instead is " + realFirst.name(), first.equals(realFirst));
		assertTrue(second + " is the spected second element of the tuple. Instead is " + realSecond, second.equals(realSecond));
		assertTrue(third + " is the spected third element of the tuple. Instead is " + realThird, third.equals(realThird));
		assertTrue(fourth + " is the spected fourth element of the tuple. Instead is " + realFourth, fourth.equals(realFourth));
		assertTrue(fifth.name() + " is the spected fifth element of the tuple. Instead is " + realFifth.name(), fifth.equals(realFifth));
	}
	
	@Test
	public void testSettersWithStatesCharactersAndStrings() {
		State first = new State("from state");
		Character second = 'c';
		Character third = 'b';
		String fourth = "cadena 1";
		State fifth = new State("to state");
		
		Quintuple<State, Character, Character, String, State> tuple = new Quintuple<State, Character, Character, String, State>(first, second, third, fourth, fifth);
		
		State newFirst = new State("new initial"),
				newFifth = new State("new final");
		
		Character newSecond = 'h',
				newThird = 'x';
		
		String newFourth = "Nueva Cadena";
		
		tuple.setFirst(newFirst);
		State actualFirst = tuple.first();
		assertTrue(newFirst.name() + " is the spected first element of the tuple. Instead is " + actualFirst.name(), newFirst.equals(actualFirst));
		
		tuple.setSecond(newSecond);
		Character actualSecond = tuple.second();
		assertTrue(newSecond + " is the spected second element of the tuple. Instead is " + actualSecond, newSecond.equals(actualSecond));
		
		tuple.setThird(newThird);
		Character actualThird = tuple.third();
		assertTrue(newThird + " is the spected third element of the tuple. Instead is " + actualThird, newThird.equals(actualThird));
		
		tuple.setFourth(newFourth);
		String actualFourth = tuple.fourth();
		assertTrue(newFourth + " is the spected fourth element of the tuple. Instead is " + actualFourth, newFourth.equals(actualFourth));
		
		tuple.setFifth(newFifth);
		State actualFifth = tuple.fifth();
		assertTrue(newFifth.name() + " is the spected first element of the tuple. Instead is " + actualFifth.name(), newFifth.equals(actualFifth));
	}
	
	@Test public void testEqualsWithStatesCharactersAndStrings() {
		State first = new State("from state");
		Character second = 'c';
		Character third = 'b';
		String fourth = "cadena 1";
		State fifth = new State("to state");
		
		State alternativeFirst = new State("new initial"),
				alternativeFifth = new State("new final");
		
		Character alternativeSecond = 'h',
				alternativeThird = 'x';
		
		String alternativeFourth = "Nueva Cadena";
		
		//Prove <A == A => A.equals(A)>
		Quintuple<State, Character, Character, String, State> originalTuple = new Quintuple<State, Character, Character, String, State>(first, second, third, fourth, fifth);
		assertTrue(originalTuple.equals(originalTuple));
		
		//Prove A, B constructed with same elements are equals.
		Quintuple<State, Character, Character, String, State> tupleCopy = new Quintuple<State, Character, Character, String, State>(first, second, third, fourth, fifth);
		assertTrue(originalTuple.equals(tupleCopy));
		
		//Prove <Quintuple A, B: A equals B in every field excepts for 1 (field1) => not(A.equals(B))>
		//field1 = first
		Quintuple<State, Character, Character, String, State> tupleDifferentFirst = new Quintuple<State, Character, Character, String, State>(alternativeFirst, second, third, fourth, fifth);
		assertFalse(originalTuple.equals(tupleDifferentFirst));
		
		//field1 = second
		Quintuple<State, Character, Character, String, State> tupleDifferentSecond = new Quintuple<State, Character, Character, String, State>(first, alternativeSecond, third, fourth, fifth);
		assertFalse(originalTuple.equals(tupleDifferentSecond));
	
		//field1 = third
		Quintuple<State, Character, Character, String, State> tupleDifferentThird = new Quintuple<State, Character, Character, String, State>(first, second, alternativeThird, fourth, fifth);
		assertFalse(originalTuple.equals(tupleDifferentThird));
		
		//field1 = fourth
		Quintuple<State, Character, Character, String, State> tupleDifferentFourth = new Quintuple<State, Character, Character, String, State>(first, second, third, alternativeFourth, fifth);
		assertFalse(originalTuple.equals(tupleDifferentFourth));
		
		//field1 = fifth
		Quintuple<State, Character, Character, String, State> tupleDifferentFifth = new Quintuple<State, Character, Character, String, State>(first, second, third, fourth, alternativeFifth);
		assertFalse(originalTuple.equals(tupleDifferentFifth));
		
		//prove not(A.equals(null))
		assertFalse(originalTuple.equals(null));
		
		//prove A.getClass() != B.getClass => not(A.equals(B))
		Object anotherClassObject = new Object();
		assertFalse(originalTuple.equals(anotherClassObject));
		
		//Prove <Quintuple A, B where A has a null field (field1) => not(A.equals(B)>
		//field1 = first
		Quintuple<State, Character, Character, String, State> tupleFirstNull = new Quintuple<State, Character, Character, String, State>(null, second, third, fourth, fifth);
		assertFalse(tupleFirstNull.equals(originalTuple));

		//field1 = second
		Quintuple<State, Character, Character, String, State> tupleSecondNull = new Quintuple<State, Character, Character, String, State>(first, null, third, fourth, fifth);
		assertFalse(tupleSecondNull.equals(originalTuple));

		//field1 = third
		Quintuple<State, Character, Character, String, State> tupleThirdNull = new Quintuple<State, Character, Character, String, State>(first, second, null, fourth, fifth);
		assertFalse(tupleThirdNull.equals(originalTuple));

		//field1 = fourth
		Quintuple<State, Character, Character, String, State> tupleFourthNull = new Quintuple<State, Character, Character, String, State>(first, second, third, null, fifth);
		assertFalse(tupleFourthNull.equals(originalTuple));

		//field1 = fifth
		Quintuple<State, Character, Character, String, State> tupleFifthNull = new Quintuple<State, Character, Character, String, State>(first, second, third, fourth, null);
		assertFalse(tupleFifthNull.equals(originalTuple));
		
		//Prove A and B has all fields null => A.equals(B)
		originalTuple = new Quintuple<State, Character, Character, String, State>(null, null, null, null, null);
		tupleCopy = new Quintuple<State, Character, Character, String, State>(null, null, null, null, null);
		assertTrue(originalTuple.equals(tupleCopy));
	}
}
