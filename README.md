## **Implementation of an Stack Automaton. The automaton is defined inside a plain text file and the path to it is passed as a parameter to the program**

#### El formato de las definiciones es el siguiente:
+ Cada transición δ(q, a, b) = (r, c) entre estados se define: q− > r[label = a/b/c];.
+ El estado inicial se define: inic[shape = point]; inic− > q0;. Donde q0 es el nombre del estado inicial.
+ Los estados finales se definen: q4[shape = doublecircle];. Donde q4 es el nombre de un estado final.
+ Los alfabetos y el conjunto de estados se deducen de la definición de δ.
+ Represente a Lambda con _
+ Represente el elemento inicial de la pila con @.
+ La primer lı́nea debe ser: digraph{ y la última }.

---
### **This project can be imported in eclipse and the next steps skiped and replaced by eclipse standards**

### **How To Build And Run Program and Tests**

+ Using a simple script
	1. cd <path/to/project>
	2. ./buildAndRunEverything.sh

### **How To Run Program and Tests**

+ Using a simple script
	1. cd <path/to/project>
	2. ./runEverything.sh

### **How To Build Program and Tests**

+ Using a simple script
	1. cd <path/to/project>
	2. ./buildEverything.sh

### **How To Run**

+ Using Provided Scripts:
	1. cd <path/to/project>
	2. ./run.sh <path/to/automaton/definition> <string to run with automaton>
+ From Terminal Manually:
	1. cd <path/to/project>
	2. java -cp bin/ automata.GenAutomatonFromFile <path/to/automaton/definition> <string to run with automaton>

---

### **How To Build**

+ Using Provided Scripts:
	1. cd <path/to/project>
	2. ./build.sh
+ From Terminal Manually:
	1. cd <path/to/project>
	2. javac -d bin/ -cp src/ src/utils/Quintuple.java src/automata/State.java src/automata/DFAPila.java src/automata/GenAutomatonFromFile.java

---

### **How To Build Tests**

+ Using Provided Scripts:
	1. cd <path/to/project>
	2. ./build_tests.sh
+ From Terminal Manually:
	1. cd <path/to/project>
	2. javac -d bin/ -cp src/:test/:lib/junit-4.12.jar:lib/hamcrest-all-1.3.jar test/utils/QuintupleTest.java test/automata/StateTest.java test/automata/DFAPilaTest.java test/suite/TestSuite.javas

---

### **How To Run Tests**

+ Using Provided Scripts:
	1. cd <path/to/project>
	2. ./run_tests.sh
+ From Terminal Manually:
	1. cd <path/to/project>
	2. java -cp bin/:lib/hamcrest-all-1.3.jar:lib/junit-4.12.jar org.junit.runner.JUnitCore suite.TestSuite
