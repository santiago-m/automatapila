package automata;

import java.util.Scanner;

public class GenAutomatonFromFile {

	public static void main(String[] args) throws Exception {
		if (args.length != 2) {
			System.out.println("Debe pasarse el path a un archivo que defina el automata y una cadena para probarla en el automata.");
		}
		else {
			String filePath = args[0];
			String str = args[1];
			DFAPila automata = DFAPila.crearDesdeArchivo(filePath);
			
			if (automata.rep_ok())
		        System.out.println("IS DFA");
			
			Scanner in = new Scanner(System.in);
			
			boolean finalStateAccepted = automata.acceptsFinalState(str);
			boolean emptyStackAccepted = automata.acceptsEmptyStack(str);
			
			
			if (finalStateAccepted && emptyStackAccepted) {
				System.out.println("El automata acepta la cadena " + str + " por Estado Final y por Pila Vacia.");
			}
			else if (finalStateAccepted && !emptyStackAccepted) {
				System.out.println("El automata acepta la cadena " + str + " por Estado Final.\n Sin embargo no la acepta por Pila Vacia.");
				System.out.println("Desea transformar el automata para que la acepte por Pila Vacia tambien? S/N");
				String resp = in.nextLine();
				
				if (resp.toLowerCase().equals("s")) {
					System.out.print("Ingrese el nombre del archivo donde desea guardar la nueva definicion del automata: ");
					String outputName = in.nextLine();
					DFAPila emptyStackAutomaton = automata.toEmptyStack();
					emptyStackAutomaton.writeToFile(outputName);
				}
				else {
					
				}
			}
			else if (!finalStateAccepted && emptyStackAccepted) {
				System.out.println("El automata acepta la cadena " + str + " por Pila Vacia.\n Sin embargo no la acepta por Estado Final.");
				System.out.println("Desea transformar el automata para que la acepte por Estado Final tambien? S/N");
				String resp = in.nextLine();
				
				if (resp.toLowerCase().equals("s")) {
					System.out.print("Ingrese el nombre del archivo donde desea guardar la nueva definicion del automata: ");
					String outputName = in.nextLine();
					DFAPila finalStateAutomaton = automata.toFinalState();
					finalStateAutomaton.writeToFile(outputName);
					
					if (finalStateAutomaton.acceptsFinalState(str))
						System.out.println("Ahora si acepta estado final");
					else 
						System.out.println("Ahora NO si acepta estado final");
				}
				else {
					System.out.println("Vuelva Pronto.");
				}
			}
			else {
				System.out.println("El automata propuesto no acepta la cadena ni por Pila Vacia ni por Estado Final.");
			}
		}
	}
}