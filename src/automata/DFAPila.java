package automata;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

import utils.Quintuple;

public final class DFAPila{

	//Constant Used to identify when to pop stack.
	public static final Character Lambda = '_';
	
    //Constant used as stack base. So it is never empty.
    public static final Character Initial = 'Z';
	
    private Stack<Character> stack; //the stack of the automaton
    
    State initial;
    Character stackInitial;
    Set<State> states;
    Set<Character> alphabet;
    Set<Character> stackAlphabet; //Alphabet of the stack
    Set<Quintuple<State,Character,Character,String,State>> transitions; //delta function
    Set<State> finalStates;


    /**
     * Constructor of the class - returns a DFAPila object
     * @param states - states of the DFAPila
     * @param alphabet - the alphabet of the automaton 
     * @param stackAlphabet - the alphabet of the stack
     * @param transitions - transitions of the automaton
     * @param stackInitial - a Character which represents the initial element of the stack
     * @param initial - initial State of the automaton
     * @param final_states - acceptance states of the automaton
     * @throws IllegalArgumentException
     */
    public DFAPila(
            Set<State> states,
            Set<Character> alphabet,
            Set<Character> stackAlphabet,
            Set<Quintuple<State, Character,Character,String, State>> transitions,
            Character stackInitial,
            State initial,
            Set<State> final_states)
            throws IllegalArgumentException 
    {
        this.states = states;
        this.alphabet = alphabet;
        this.alphabet.add(Lambda);
        this.stackAlphabet = stackAlphabet;
        this.stackAlphabet.add(Lambda); //the character lambda is used in the stack to know when do a pop
        //this.stackAlphabet.add(Joker); //the mark of the stack
        this.stackAlphabet.add(Initial);
        this.stackAlphabet.add(stackInitial);
        this.transitions = transitions;
        this.stackInitial = stackInitial;
        this.initial = initial;
        this.finalStates = final_states;
        stack = new Stack<Character>();
        stack.push(Initial);
        stack.push(stackInitial);	//insert the first element known for the user in stack on top of it.
        if (!rep_ok()) {
            throw new  IllegalArgumentException();
        }
    }
    
    /**
     * Search in the set of transitions, that which:
     * 					+ First element is the state "from".
     * 					+ Second is the character "c".
     * 					+ Third corresponds with the current top of the stack.
     * 
     * Then returns that transition if exists. Else return null
     * */
    
    public State delta(State from, Character c) {
    	
		for (Quintuple<State, Character, Character, String, State> e : transitions) {
			
			String fname = ((State) e.first()).name(), foname = (String) e.fourth(), finame = ((State) e.fifth()).name();
			Character sname = (Character) e.second(), tname = (Character) e.third();
			
			//Si la transicion va desde from consumiendo c
			if ((e.first().equals(from)) && (e.second().equals(c))) {
				
				//Si ademas, el tope de la pila requerido en la transicion es el actual
				if (stack.peek().equals(e.third())) {
					
					//Se aplican los cambios en la pila
					stack.pop();
					String itemsToStack = (String) e.fourth();
					
					Character firstItemToStack = new Character(itemsToStack.charAt(0));
					if (!firstItemToStack.equals(Lambda)) {
						for (int i = itemsToStack.length() - 1; i >= 0; i--) {
							stack.push(itemsToStack.charAt(i));
						}
					}

					//retorna entonces el estado
					return (State) e.fifth();
				}
			}
		}
    	return null;
    }
    
    /**
     * Use delta method to run a sequence of chars S in DFAPila.
     * 			1) assign to newstate the result of delta with initial state and first char of S.
     * 			2) assign to newstate the result of delta with newstate and next char of S.
     * 			3) if newstate is null => assign to it the result of delta with last not null state and Lambda.  
     * 			4) while newstate != null => go back to 2)
     * 
     *   returns true if last reached state is a Final State AND all sequence elements were consumed.
     * */
        
    public boolean acceptsFinalState(String string) {
    	State current = initial;
    	State aux = current;
    	int charIndex = 0;
        
    	//Se actualiza el estado segun delta
    	do {
    		//Update state with current char of string.
    		if (charIndex < string.length())
    			aux = delta(current, string.charAt(charIndex));
    		else
    			aux = null;
    		
    		if (aux != null) {
    			current = aux;
    			charIndex++;
    		}
    		//If new state is null, search for lambda transitions from current state and current stack top and update new state.
    		else {
    			aux = delta(current, Lambda);
    			
    			if (aux != null)
    				current = aux;
    		}
    	} while (aux != null);

    	//The stack is reseted to its initial state.
    	stack = new Stack<Character>();
    	stack.push(Initial);
        stack.push(stackInitial); //insert the mark in the stack
        
        //return true if current belongs to finalStates and all the string was consumed.
    	return finalStates.contains(current) && charIndex >= string.length();
    }

    /**
     * Use delta method to run a sequence of chars S in DFAPila.
     * 			1) assign to newstate the result of delta with initial state and first char of S.
     * 			2) assign to newstate the result of delta with newstate and next char of S.
     * 			3) if newstate is null => assign to it the result of delta with last not null state and Lambda.  
     * 			4) while newstate != null => go back to 2)
     * 
     *   returns true if once all sequence elements were consumed, then the stack is empty, i.e only Initial is on stack.
     * */
    
    public boolean acceptsEmptyStack(String string) {
    	State current = initial;
    	State aux = current;
    	int charIndex = 0;
        
    	//Se actualiza el estado segun delta
    	do {
    		if (charIndex < string.length())
    			aux = delta(current, string.charAt(charIndex));
    		else
    			aux = null;
    		
    		if (aux != null) {
    			current = aux;
    			charIndex++;
    		}
    		else {
    			aux = delta(current, Lambda);
    			
    			if (aux != null)
    				current = aux;
    		}
    	} while (aux != null);
    	
    	boolean res;
    	
    	//Si el tope de la pila es el inicial y ademas se consumio toda la cadena
    	if (stack.pop().equals(Initial) && charIndex >= string.length())
    		res = true;
    	else 
            res = false;
    	
		//Se reinicia la pila a su estado original.
    	stack = new Stack<Character>();
    	stack.push(Initial);
        stack.push(stackInitial); //insert the mark in the stack
        
        return res;
    }

    /**
     * The rep_ok method which check if the object is a correct instance of DFAPila. 
     * */
    
    public boolean rep_ok() {
    	//If some element of DFAPila is null. returns false.
    	if ((initial == null) || (stackInitial == null) || (states == null) || (alphabet == null) || (stackAlphabet == null) || (transitions == null) || (finalStates == null))
    	    return false;

        if (initial.name() == null) 
        	return false;
        
        if (Initial.charValue() == stackInitial.charValue()) 
        	return false;
        
        if ( !(stackAlphabet.contains(stackInitial)) || !(stackAlphabet.contains(Lambda)) || !(stackAlphabet.contains(Initial))) 
        	return false;
        
        if (stack.contains(Lambda)) 
        	return false;
        
        if (!states.containsAll(finalStates)) 
        	return false;
        
        for (Quintuple e : transitions) {
        	//If some element of any transition is null, returns false.
        	if ( (e.first() == null) || (e.second() == null) || (e.third() == null) || (e.fourth() == null) || (e.fifth() == null) )
        		return false;
        	
        	State fromState = (State) e.first(), toState = (State) e.fifth();
        	Character charToEat = (Character) e.second(), topOfStack = (Character) e.third();
        	String charsToStack = (String) e.fourth();
        	
        	if ( (!states.contains(fromState)) || (!states.contains(toState)) )
        		return false;
        	
        	if (!alphabet.contains(charToEat))
        		return false;

        	if (!stackAlphabet.contains(topOfStack))
        		return false;
        	
        	for (int i = 0; i < charsToStack.length(); i++) {
        		if (!stackAlphabet.contains(charsToStack.charAt(i)))
        			return false;
        	}
        }
    	return true;
    }

    /**
     * Build a new automaton based on this instance, 
     * which accepts the same strings that this does by empty stack, but by final state.  
     * */
    public DFAPila toFinalState() {
    	//Creo nuevos estados inicial y final. Los agrego a un nuevo set de estados.
    	State newInitial = new State("new_initial");
    	State newFinal = new State("new_final");
    	
    	Set<State> newStates = new HashSet<State>();
    	newStates.addAll(states);
    	newStates.add(newInitial);
    	newStates.add(newFinal);
    	
    	Set<State> newFinalStates = new HashSet<State>();
    	newFinalStates.add(newFinal);
    	
    	//Creo un nuevo stackInitial y lo agrego al nuevo alfabeto de stack.
    	Character newStackMark = '-';
    	
    	Set<Character> newStackAlphabet = new HashSet<Character>();
    	newStackAlphabet.addAll(stackAlphabet);
    	newStackAlphabet.add(newStackMark);
    	
    	//Creo las nuevas transiciones y las agrego al nuevo set de transiciones.
    	
    	//Agrego al nuevo set las transiciones viejas
    	Set<Quintuple<State,Character,Character,String,State>> newTransitions = new HashSet<Quintuple<State,Character,Character,String,State>>();
    	newTransitions.addAll(transitions);
    	
    	//Reemplazo en el nuevo set, todas las ocurrencias del inicial viejo por la nueva marca de stack
    	for (Quintuple<State,Character,Character,String,State> t : newTransitions) {
    		if (t.third().equals(Initial))
    			t.setThird(newStackMark);
    		for (int i = 0; i < t.fourth().length(); i++)
    			if (t.fourth().charAt(i) == Initial.charValue())
    				t.setFourth(t.fourth().replace(Initial, newStackMark));
    	}
    	
    	//Agrego la transicion del nuevo inicial al viejo
    	Quintuple<State,Character,Character,String,State> newAuxTransition = new Quintuple<State,Character,Character,String,State>(newInitial, Lambda, stackInitial, stackInitial + newStackMark.toString(), initial);
    	newTransitions.add(newAuxTransition);
    	
    	//Agrego las transiciones de todos los estados viejos al nuevo final.
    	for (State s: states) {
    		newAuxTransition = new Quintuple<State,Character,Character,String,State>(s, Lambda, newStackMark, Lambda.toString(), newFinal);
    		newTransitions.add(newAuxTransition);
    	}
    	
    	//Construyo el nuevo automata
    	DFAPila finalStateAutomaton = new DFAPila(newStates, alphabet, newStackAlphabet, newTransitions, stackInitial, newInitial, newFinalStates);
    	
    	return finalStateAutomaton;
    }
    
    /**
     * Build a new automaton based on this instance, 
     * which accepts the same strings that this does by final state, but by empty stack.  
     * */
    public DFAPila toEmptyStack() {
    	//Creo nuevos estados inicial y final, este ultimo para vaciar la pila. Los agrego a un nuevo set de estados.
    	State newInitial = new State("new_initial");
    	State newDrainState = new State("drain_state");
    	
    	Set<State> newStates = new HashSet<State>();
    	newStates.addAll(states);
    	newStates.add(newInitial);
    	newStates.add(newDrainState);

    	//Agrego drain state a un nuevo conjunto de estados finales. 
    	//Asi el nuevo automata acepta las cadenas tanto por cola vacia como por estado final.
    	Set<State> newFinalStates = new HashSet<State>();
    	newFinalStates.add(newDrainState);
    	
    	//Creo un nuevo stackInitial y lo agrego al nuevo alfabeto de stack.
    	Character newStackMark = '-';
    	
    	Set<Character> newStackAlphabet = new HashSet<Character>();
    	newStackAlphabet.addAll(stackAlphabet);
    	newStackAlphabet.add(newStackMark);
    	
    	//Creo las nuevas transiciones y las agrego al nuevo set de transiciones.
    	
    	//Agrego al nuevo set las transiciones viejas
    	Set<Quintuple<State,Character,Character,String,State>> newTransitions = new HashSet<Quintuple<State,Character,Character,String,State>>();
    	newTransitions.addAll(transitions);
    	
    	//Reemplazo en el nuevo set, todas las ocurrencias del inicial viejo por la nueva marca de stack
    	for (Quintuple<State,Character,Character,String,State> t : newTransitions) {
    		if (t.third().equals(Initial))
    			t.setThird(newStackMark);
    		for (int i = 0; i < t.fourth().length(); i++)
    			if (t.fourth().charAt(i) == Initial.charValue())
    				t.setFourth(t.fourth().replace(Initial, newStackMark));
    	}
    	
    	//Agrego la transicion del nuevo inicial al viejo
    	Quintuple<State,Character,Character,String,State> newAuxTransition = new Quintuple<State,Character,Character,String,State>(newInitial, Lambda, stackInitial, stackInitial + newStackMark.toString(), initial);
    	newTransitions.add(newAuxTransition);
    	
    	//Agrego las transiciones de los estados finales al estado vaciador.
    	for (State s: finalStates) {
    		for (Character c : stackAlphabet) 
    			if (c != Initial) {
    				newAuxTransition = new Quintuple<State,Character,Character,String,State>(s, Lambda, c, Lambda.toString(), newDrainState);
        			newTransitions.add(newAuxTransition);
    			}
    		newAuxTransition = new Quintuple<State,Character,Character,String,State>(s, Lambda, newStackMark, Lambda.toString(), newDrainState);
			newTransitions.add(newAuxTransition);
    	}
    	
    	//Agrego las transiciones de faltantes del estado vaciador a si mismo. -para vaciar la pila-
    	for (Character c : stackAlphabet)
    		if (c != Initial) {
				newAuxTransition = new Quintuple<State,Character,Character,String,State>(newDrainState, Lambda, c, Lambda.toString(), newDrainState);
    			newTransitions.add(newAuxTransition);
			}
    	newAuxTransition = new Quintuple<State,Character,Character,String,State>(newDrainState, Lambda, newStackMark, Lambda.toString(), newDrainState);
		newTransitions.add(newAuxTransition);
    		
    	
    	//Construyo el nuevo automata
    	DFAPila emptyStackAutomaton = new DFAPila(newStates, alphabet, newStackAlphabet, newTransitions, stackInitial, newInitial, newFinalStates);
    	
    	return emptyStackAutomaton;
    }
    
    /**
     * Build a DFAPila from file. i.e from a plain text definition from automaton.
     * Then returns it.  
     * */
    
    public static DFAPila crearDesdeArchivo(String filePath) throws FileNotFoundException {
		File file = new File(filePath);
	    Scanner sc = new Scanner(file);
	    
	    List<String> stateNames = new ArrayList<String>();
	    State initialState = null;
	    Set<State> states = new HashSet<State>();
	    Set<State> final_states = new HashSet<State>();
	    Set<Character> alphabet = new HashSet<Character>();
	    Set<Character> stackAlphabet = new HashSet<Character>();
	    Character stackInitial = null;
	    Set<Quintuple<State, Character,Character,String, State>> transitions = new HashSet<Quintuple<State, Character,Character,String, State>>();
	 
	    //Reads the file line by line.
	    while (sc.hasNextLine()) {
	    	String currentLine = sc.nextLine();
	    	
	    	//If the current line is a a transition.
	    	if (currentLine.matches("^(.)*->(.)*")) {
	    		
	    		//Split the transition in left and right parts.
	    		String[] substrings = currentLine.split("->");
	    		
	    		//fromStateName is left side of transition.
	    		String fromStateName = substrings[0];
	    		//toStateName is right side of transition.
	    		String toStateName = substrings[1].substring(0, substrings[1].length()-1);
	    		
	    		Character alphabetChar = null;
	    		Character stackAlphabetChar = null;
	    		String strToStack = null;
	    		
	    		//Now splits righ part of transition by "label" string
	    		//which define the character to consume, the top of stack required and the items to stack after transition.
	    		substrings = toStateName.split("(\\s)*.label.\"");

	    		//Maintain in toStateName only the name of the right side state.
	    		toStateName = substrings[0];
	    		
	    		State from = null, to = null;
	    		
	    		//If left side state is "inic". Then right side state is the initial state of automaton.
	    		if (fromStateName.equals("inic")) {
	    			initialState = new State(toStateName);
	    			to = initialState;
	    		}
	    		//Else add to the set of states both left side and right side states.
	    		//Only if some of these are not in the set yet.
	    		if ((!fromStateName.equals("inic")) && (!stateNames.contains(fromStateName))) {
	    			stateNames.add(fromStateName);
	    			from = new State(fromStateName);
	    			states.add(from);
	    		}
	    		if (!stateNames.contains(toStateName)) {
	    			stateNames.add(toStateName);
	    			if (to == null)
	    				to = new State(toStateName);
	    			states.add(to);
	    		}
	    		
	    		//Then analyze the label section of transition
	    		if (substrings.length > 1) {
	    			
	    			//Split the section in
	    			//		+ Char to consume.
	    			//		+ Char that is required at top of stack.
	    			//		+ Chars to stack.
	    			substrings = substrings[1].split("/");
	    			
	    			//Char to consume.
	    			alphabetChar = substrings[0].charAt(0);
	    			//Char that is required at top of stack.
	    			stackAlphabetChar = substrings[1].charAt(0);
	    			//Chars to stack.
	    			strToStack = substrings[2].substring(0, substrings[2].length() - 2);
	    			
	    			//If left side state is the initial state,
	    			//Then the stackInitial char is obtained from label section as the required char in stack.
	    			if (fromStateName.equals(initialState.name()))
	    				stackInitial = stackAlphabetChar;
	    			
	    			//Adds the char to consume by the transition to the alphabet, only if it is not already in it.
	    			if (!alphabet.contains(alphabetChar))
	    				alphabet.add(alphabetChar);

	    			////Adds the stack top required by the transition to the stack alphabet, only if its not Lambda char and it is not already in it.
	    			if ((!stackAlphabetChar.equals(DFAPila.Lambda)) && (!stackAlphabet.contains(stackAlphabetChar)))
	    				stackAlphabet.add(stackAlphabetChar);
	    			
	    			//Adds all the chars to stack to the stackAlphabet as long as the current char is not Lambda.
	    			for (int i = 0; i < strToStack.length(); i++) {
	    				Character aux = strToStack.charAt(i);
	    				if ((!aux.equals(DFAPila.Lambda)) && (!stackAlphabet.contains(aux)))
		    				stackAlphabet.add(aux);
	    			}	    			
	    			
	    			//Creates the origin and end states to create the transition.
	    			if ((from == null) || (to == null)) {
	    				for (State s : states) {
	    					if ( (from == null) && (s.name().equals(fromStateName)) ) {
	    						from = s;
	    					}
	    					if ( (to == null) && (s.name().equals(toStateName)) ) {
	    						to = s;
	    					}
	    				}
	    			}
	    			
	    			//Create the transition read by using Quintuple class constructor.
	    			Quintuple<State,Character,Character,String,State> transition = new Quintuple<State,Character,Character,String,State>(from, alphabetChar, stackAlphabetChar, strToStack, to);
	    			
	    			//Add the new transition to set of transitions.
	    			transitions.add(transition);
	    			
	    		}
	    	}
	    	//Else if the current line is not a transition but a final state assignment.
	    	else if (currentLine.matches("(.)*.shape(.)*doublecircle(.)*")) {
	    		
	    		//Split the line with "[shape=" regex.
	    		String[] substrings = currentLine.split(".shape.");
	    		
	    		//Left part of line is noew the final state name.
	    		State finalS = new State(substrings[0]);
	    		
	    		//If this state is not already in state set, then adds it.
	    		if (!stateNames.contains(substrings[0])) {
	    			stateNames.add(substrings[0]);
	    			states.add(finalS);
	    			final_states.add(finalS);
	    		}
	    		//If it was already created and added to states, then search for it and adds it to final states set.
	    		else {
	    			for (State s : states) {
	    				if (s.name().equals(substrings[0])) {
	    					final_states.add(s);
	    				}
	    			}
	    		}
	    	}
	    } 
	    
	    DFAPila result;
	    
	    //Try to create the automaton with the collected data. Then returns it.
	    try {
	    	result = new DFAPila(states, alphabet, stackAlphabet, transitions, stackInitial, initialState, final_states);
	    }
	    catch (java.lang.IllegalArgumentException e) {
	    	e.printStackTrace();
	    	result = null;
	    }
	    return result;
	}
    
    /**
     * Write this instance definition to file. i.e, a plain text definition 
     * which can be used by crearDesdeArchivo method.  
     * */
    
    public void writeToFile(String outputPath) {
    	BufferedWriter writer = null;
        try {
            //create a file
            File newAutomatonFile = new File(outputPath);

            // This write the automaton definition to file.
            writer = new BufferedWriter(new FileWriter(newAutomatonFile));
            writer.write("digraph{\n");
            writer.write("inic[shape=point];\n");
            writer.write("inic->" + initial.name() + ";\n");
            
            for (Quintuple<State,Character,Character,String,State> t : transitions) {
            	String from = t.first().name();
            	String to = t.fifth().name();
            	String label = t.second() + "/" + t.third() + "/" + t.fourth();
            	
            	writer.write(from + "->" + to + " [label=\"" + label + "\"];\n");
            }
            for (State s : finalStates) {
            	writer.write(s.name() + "[shape=doublecircle];\n");
            	writer.write("}\n");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens
                writer.close();
            } catch (Exception e) {
            }
        }
    }
    
  /**
   * This method returns true if this instance of DFAPila is deterministic automaton.
   * */

    private boolean esDeterministico() {
    	boolean res = true;
    	for (Quintuple<State, Character, Character, String, State> t : transitions) {
    		for (Quintuple<State, Character, Character, String, State> t2 : transitions) {
    			if (t.first().equals(t2.first()))
    				if (t.second().equals(t2.second()))
    					if (t.third().equals(t2.third())) {
    						res = t.fourth().equals(t2.fourth()) && t.fifth().equals(t2.fifth());
    						if (!res)
    							break;
    					}
    				
    		}
    	}
    	return res;
    }
    
    /**
     * This is an override of equals method for this particular class implementation.
     * */
    @Override
    public boolean equals(Object obj) {
    	//Return false if obj is not a DFAPila instance
    	if (!(obj instanceof DFAPila))
    		return false;
    	
    	DFAPila other = (DFAPila) obj;
    	
    	//Return false if size of states is different from obj
    	if (states.size() != other.states.size())
    		return false;
    	
    	//Count the quantity of states wich are in this instance and in obj states.
    	int cantEstadosEnComun = 0;
    	
    	for (State s : states)
    		for (State s_other : other.states)
    			if (s.name().equals(s_other.name())) {
    				cantEstadosEnComun++;
    				
    				//Return false if the current state s is also a final state but its equivalent in obj is not a final state
    				if (finalStates.contains(s) && !other.finalStates.contains(s_other))
    					return false;
    				
    				//Or viceverse
    				if (!finalStates.contains(s) && other.finalStates.contains(s_other))
    					return false;
    				
    			}
    	//Return false not all of the states are common for both instances
    	if (cantEstadosEnComun != states.size())
    		return false;
    	
    	//Return false if at least one character of alphabet is not in other alphabet
    	for (Character c : alphabet)
    		if (!other.alphabet.contains(c))
    			return false;
    	
    	//Return false if at least one character of stackAlphabet is not in other stackAlphabet
    	for (Character c : stackAlphabet)
    		if (!other.stackAlphabet.contains(c)) {
    			

    			return false;
    		}
    	
    	//Return false if initial is not the same as others initial
    	if (!initial.equals(other.initial))
    		return false;
    	
    	//Return false if stackInitial is not the same as others stackInitial
    	if (!stackInitial.equals(other.stackInitial))
    		return false;
    	
    	//Return false if quantity of transitions in this instance is different from other instance
    	if (transitions.size() != other.transitions.size())
    		return false;
    	
    	//Return false if at least one transition of this instance is not in other instance
    	int cantDeTransicionesEnComun = 0;
    	
    	//Count quantity of transitions that are common to both instances
    	for (Quintuple<State, Character, Character, String, State> t : transitions)
    		for (Quintuple<State, Character, Character, String, State> t_other : other.transitions)
    			if (
    					t.first().name().equals(t_other.first().name()) &&
    					t.second().equals(t_other.second()) &&
    					t.third().equals(t_other.third()) &&
    					t.fourth().equals(t_other.fourth()) &&
    					t.fifth().name().equals(t_other.fifth().name())
    					)
    				cantDeTransicionesEnComun++;
    				
    	//Return false if at least one transition is not common to both of instances
    	if (cantDeTransicionesEnComun != transitions.size())
    		return false;
    	
    	return esDeterministico();
    }
	
}
