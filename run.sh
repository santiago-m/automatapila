if [ $# -eq 0 ] || [ $# -eq 1 ] 
  then
    echo "Debe pasar los siguientes parametros en el orden descripto:"
    echo "	1) El path a una definicion de automata pila para correr el programa."
    echo "	   existe un archivo de prueba en la carpeta automatonDefinitions"
    echo "	2) Una cadena para probarla con el automata creado"
    echo ""
    echo ""

  	echo "• Cada transicion δ(q, a, b) = (r, c) entre estados se define: q− > r[label = a/b/c];."
		echo "• El estado inicial se define: inic[shape = point];inic− > q0;. Donde q0 es el nombre del estado inicial."
		echo "• Los estados finales se definen: q4[shape = doublecircle];. Donde q4 es el nombre de un estado final."
		echo "• Los alfabetos y el conjunto de estados se deducen de la definicion de δ."
		echo "• Represente a 'la cadena vacia' con _"
		echo "• Represente el elemento inicial de la pila con @."
		echo "• La primer linea debe ser: digraph{ y la ´ultima }"

  else
  	java -cp bin/ automata.GenAutomatonFromFile $1 $2
fi
